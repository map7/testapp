package ca.agyl.testapp.entities;

import java.util.ArrayList;
import java.util.List;

public class SyncItem {

    public static final int PHOTOS_MAX_NUMBER = 25;
    public static final int NOTES_MAX_NUMBER = 3;
    public static final int FORMS_MAX_NUMBER = 2;

    private String mId;

    private String mLastName;

    private String mFirstName;

    // Default init values
    private int mPhotos = 0;

    private int mNotes = 0;

    private int mForms = 0;

    private boolean mCompleted = false;

    public SyncItem(String id, String lastName, String firstName) {
        this.mId = id;
        this.mLastName = lastName;
        this.mFirstName = firstName;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public int getPhotos() {
        return mPhotos;
    }

    public void setPhotos(int photos) {
        this.mPhotos = photos;
    }

    public int getNotes() {
        return mNotes;
    }

    public void setNotes(int notes) {
        this.mNotes = notes;
    }

    public int getForms() {
        return mForms;
    }

    public void setForms(int forms) {
        this.mForms = forms;
    }

    public boolean isCompleted() {
        return mPhotos == PHOTOS_MAX_NUMBER
                && mNotes == NOTES_MAX_NUMBER
                && mForms == FORMS_MAX_NUMBER;
    }

    // Method to create dummy data
    public static List<SyncItem> createSyncItemsList(int times) {

        List<SyncItem> syncItems = new ArrayList<>();

        for (int i = 0; i < times; i++) {

            SyncItem item1 = new SyncItem("DOS-00045545", "Nom", "Prénom");
            item1.setPhotos(25);
            item1.setNotes(3);
            item1.setForms(2);

            syncItems.add(item1);
        }

        return syncItems;
    }

}
