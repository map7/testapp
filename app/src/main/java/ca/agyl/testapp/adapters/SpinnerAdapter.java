package ca.agyl.testapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import ca.agyl.testapp.R;

public class SpinnerAdapter extends ArrayAdapter {

    public SpinnerAdapter(Context context, Object[] objects) {
        super(context, R.layout.menu_spinner_item, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        view.setPadding(0, view.getPaddingTop(), view.getPaddingRight(), view.getPaddingBottom());
        return view;
    }
}
