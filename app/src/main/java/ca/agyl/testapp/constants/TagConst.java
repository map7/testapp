package ca.agyl.testapp.constants;

public class TagConst {

    // Fragment Tags
    public static final String SYNC_LIST_FRAGMENT_TAG = "SyncListTag";
    public static final String FILE_LIST_FRAGMENT_TAG = "FileListTag";

}
