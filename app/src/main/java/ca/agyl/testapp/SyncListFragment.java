package ca.agyl.testapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import ca.agyl.testapp.adapters.SyncListAdapter;
import ca.agyl.testapp.custom.ListDividerItemDecoration;
import ca.agyl.testapp.entities.SyncItem;

public class SyncListFragment extends Fragment {

    private static final String LOG_TAG = SyncListFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static SyncListFragment newInstance(Bundle args) {
        SyncListFragment f = new SyncListFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sync_list, container, false);

        setHasOptionsMenu(true);

        // Update the toolbar title
        ((MainActivity) getActivity()).getCollapsingToolbarLayout().setTitle(getString(R.string.sync_toolbar_title));

        initViews(rootView);
        configViews();

        return rootView;
    }

    private void initViews(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.sync_list_recycler_view);
    }

    private void configViews() {
        // Use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // Use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Add the line divider
        mRecyclerView.addItemDecoration(new ListDividerItemDecoration(getContext()));

        // Create and set the adapter
        mAdapter = new SyncListAdapter(getActivity(), SyncItem.createSyncItemsList(10));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_sync_list, menu);
    }
}
