package ca.agyl.testapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import ca.agyl.testapp.adapters.FileListAdapter;
import ca.agyl.testapp.custom.ListDividerItemDecoration;
import ca.agyl.testapp.entities.FileItem;

public class FileListFragment extends Fragment {

    private static final String LOG_TAG = FileListFragment.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public static FileListFragment newInstance(Bundle args) {
        FileListFragment f = new FileListFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_file_list, container, false);

        setHasOptionsMenu(true);

        // Update the toolbar title
        ((MainActivity) getActivity()).getCollapsingToolbarLayout().setTitle(getString(R.string.files_toolbar_title));

        initViews(rootView);
        configViews();

        return rootView;
    }

    private void initViews(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.file_list_recycler_view);
    }

    private void configViews() {
        // Use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // Use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        // Create and set the adapter
        mAdapter = new FileListAdapter(getActivity(), FileItem.createfileItemsList(10));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_file_list, menu);
    }

}
