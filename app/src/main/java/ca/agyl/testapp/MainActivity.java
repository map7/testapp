package ca.agyl.testapp;

import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


import ca.agyl.testapp.adapters.SpinnerAdapter;
import ca.agyl.testapp.constants.TagConst;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    public boolean mNavMenuEnabled;

    private CollapsingToolbarLayout mCollapsingToolbarLayout;

    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private static final String SELECTED_MENU_ITEM_ID_KEY = "SelectedMenuItemIdKey";
    private int selectedMenuItemId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        mNavMenuEnabled = true;

        if (savedInstanceState == null) {
            // Init the MapUiFragment
            FileListFragment fileListFragment = FileListFragment.newInstance(null);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fileListFragment, TagConst.FILE_LIST_FRAGMENT_TAG)
                    .commit();
        } else {
            if (savedInstanceState.containsKey(SELECTED_MENU_ITEM_ID_KEY)) {
                selectedMenuItemId = savedInstanceState.getInt(SELECTED_MENU_ITEM_ID_KEY);
            }
        }

        initDrawerMenu();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_MENU_ITEM_ID_KEY, selectedMenuItemId);
    }

    protected void initDrawerMenu() {

        // Initialize the Drawer List
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Set the menu's click listener
        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem item) {

                        // Uncheck all menu items
                        Menu menu = mNavigationView.getMenu();
                        for (int i = 0; i < menu.size(); i++) {
                            menu.getItem(i).setChecked(false);
                        }

                        selectedMenuItemId = item.getItemId();
                        selectMenuItem(item);
                        item.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                }
        );

        Log.v(LOG_TAG, "selectedMenuItemId: " + selectedMenuItemId);

        if (selectedMenuItemId != -1) {
            mNavigationView.getMenu().findItem(selectedMenuItemId).setChecked(true);
        } else {
            // Automatically check the first item "Dossiers"
            mNavigationView.getMenu().getItem(0).setChecked(true);
        }

        // Set the menu icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.string.drawer_open,
                R.string.drawer_close
        );

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.icon_menu);

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        // User Email Spinner
        configMenuSpinner();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /** Swaps fragments in the main content view */
    private boolean selectMenuItem(MenuItem item) {

        Log.d(LOG_TAG, "selectMenuItem item: " + item);

        int id = item.getItemId();

        if (id == R.id.menu_action_sync) {

            // Check to not reload if the fragment is already opened.
            SyncListFragment syncListFragment = (SyncListFragment) getSupportFragmentManager()
                    .findFragmentByTag(TagConst.SYNC_LIST_FRAGMENT_TAG);

            if (syncListFragment != null && syncListFragment.isAdded()) {
                return true;
            }

            SyncListFragment f = SyncListFragment.newInstance(null);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, f, TagConst.SYNC_LIST_FRAGMENT_TAG)
                    .commit();

            return true;
        }


        if (id == R.id.menu_action_dossiers) {

            // Check to not reload if the fragment is already opened.
            FileListFragment fileListFragment = (FileListFragment) getSupportFragmentManager()
                    .findFragmentByTag(TagConst.FILE_LIST_FRAGMENT_TAG);

            if (fileListFragment != null && fileListFragment.isAdded()) {
                return true;
            }

            FileListFragment f = FileListFragment.newInstance(null);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, f, TagConst.FILE_LIST_FRAGMENT_TAG)
                    .commit();

            return true;
        }

        return false;
    }

    private void configMenuSpinner() {
        View headerView = mNavigationView.getHeaderView(0);
        Spinner spinner = (Spinner) headerView.findViewById(R.id.user_email_spinner);

        Drawable spinnerDrawable = spinner.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        spinner.setBackground(spinnerDrawable);

        ArrayAdapter adapter = new SpinnerAdapter(
                this,
                new String[]{"prenom.nom@gus.ca"}
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public CollapsingToolbarLayout getCollapsingToolbarLayout() {
        return mCollapsingToolbarLayout;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mNavMenuEnabled) {
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mNavMenuEnabled) {
            if (isNavDrawerOpen()) {
                closeNavDrawer();
            }
        } else {
            super.onBackPressed();
        }
    }


    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }


    protected void closeNavDrawer() {
        if (mNavMenuEnabled) {
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mNavMenuEnabled) mDrawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mNavMenuEnabled) mDrawerToggle.onConfigurationChanged(newConfig);
    }

}
