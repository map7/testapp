package ca.agyl.testapp.entities;

import java.util.ArrayList;
import java.util.List;

public class FileItem {

    private String mAddress;

    private String mLastName;

    private String mFirstName;

    private int mFileCode;

    private String mFileDescription;


    public FileItem(String address, String lastName, String firstName, int fileCode, String fileDescription) {
        this.mAddress = address;
        this.mLastName = lastName;
        this.mFirstName = firstName;
        this.mFileCode = fileCode;
        this.mFileDescription = fileDescription;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public int getFileCode() {
        return mFileCode;
    }

    public void setFileCode(int fileCode) {
        this.mFileCode = fileCode;
    }

    public String getFileDescription() {
        return mFileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.mFileDescription = fileDescription;
    }

    // Method to create dummy data
    public static List<FileItem> createfileItemsList(int times) {

        List<FileItem> fileItems = new ArrayList<>();

        for (int i = 0; i < times; i++) {

            FileItem item =
                    new FileItem(
                            "175 de l'Église, St-Augustin-de-Desmaures",
                            "Nom",
                            "Prénom",
                            10039,
                            "Estimation, Vent");

            fileItems.add(item);

        }
        return fileItems;
    }

}
