package ca.agyl.testapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ca.agyl.testapp.R;
import ca.agyl.testapp.entities.FileItem;
import ca.agyl.testapp.entities.SyncItem;

public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.ViewHolder> {

    private Context mContext;
    private List<FileItem> mData;


    public FileListAdapter(Context context, List<FileItem> data) {

        mContext = context;
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View syncListItem = inflater.inflate(R.layout.file_list_item, parent, false);

        return new ViewHolder(syncListItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        FileItem fileItem = mData.get(position);
        holder.mAddressTextView.setText(fileItem.getAddress());
        holder.mLastNameTextView.setText(fileItem.getLastName());
        holder.mFirstNameTextView.setText(fileItem.getFirstName());
        holder.mFileCodeTextView.setText(Integer.toString(fileItem.getFileCode()));
        holder.mFileDescriptionTextView.setText(fileItem.getFileDescription());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mAddressTextView;
        public TextView mLastNameTextView;
        public TextView mFirstNameTextView;
        public TextView mFileCodeTextView;
        public TextView mFileDescriptionTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            mAddressTextView = (TextView) itemView.findViewById(R.id.file_item_address);
            mLastNameTextView = (TextView) itemView.findViewById(R.id.file_person_lastname);
            mFirstNameTextView = (TextView) itemView.findViewById(R.id.file_person_firstname);
            mFileCodeTextView = (TextView) itemView.findViewById(R.id.file_item_code);
            mFileDescriptionTextView = (TextView) itemView.findViewById(R.id.file_item_description);
        }
    }

}
