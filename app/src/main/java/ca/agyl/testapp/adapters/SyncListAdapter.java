package ca.agyl.testapp.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ca.agyl.testapp.R;
import ca.agyl.testapp.entities.SyncItem;

public class SyncListAdapter extends RecyclerView.Adapter<SyncListAdapter.ViewHolder> {

    private Context mContext;
    private List<SyncItem> mData;


    public SyncListAdapter(Context context, List<SyncItem> data) {

        mContext = context;
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View syncListItem = inflater.inflate(R.layout.sync_list_item, parent, false);

        return new ViewHolder(syncListItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        SyncItem syncItem = mData.get(position);

        holder.mIdTextView.setText(syncItem.getId());
        holder.mLastNameTextView.setText(syncItem.getLastName());
        holder.mFirstNameTextView.setText(syncItem.getFirstName());

        holder.mPhotosStatusTextView.setText(String.format(
                mContext.getString(R.string.sync_item_photos_status),
                syncItem.getPhotos(),
                SyncItem.PHOTOS_MAX_NUMBER));

        holder.mNotesStatusTextView.setText(String.format(
                mContext.getString(R.string.sync_item_notes_status),
                syncItem.getNotes(),
                SyncItem.NOTES_MAX_NUMBER));

        holder.mFormsStatusTextView.setText(String.format(
                mContext.getString(R.string.sync_item_forms_status),
                syncItem.getForms(),
                SyncItem.FORMS_MAX_NUMBER));

        if (syncItem.isCompleted()) {
            holder.mOverallSyncStatusImage.setImageDrawable(
                    ContextCompat.getDrawable(mContext, R.drawable.icon_status_completed));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mIdTextView;
        public TextView mLastNameTextView;
        public TextView mFirstNameTextView;
        public TextView mPhotosStatusTextView;
        public TextView mNotesStatusTextView;
        public TextView mFormsStatusTextView;
        public ImageView mOverallSyncStatusImage;

        public ViewHolder(View itemView) {
            super(itemView);

            mIdTextView = (TextView) itemView.findViewById(R.id.sync_item_id);
            mLastNameTextView = (TextView) itemView.findViewById(R.id.sync_person_lastname);
            mFirstNameTextView = (TextView) itemView.findViewById(R.id.sync_person_firstname);
            mPhotosStatusTextView = (TextView) itemView.findViewById(R.id.sync_photos_status);
            mNotesStatusTextView = (TextView) itemView.findViewById(R.id.sync_notes_status);
            mFormsStatusTextView = (TextView) itemView.findViewById(R.id.sync_forms_status);
            mOverallSyncStatusImage = (ImageView) itemView.findViewById(R.id.sync_item_status_indicator);

        }
    }

}
